import React from "react";
import Box from "@material-ui/core/Box";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import "./style.css";

const theme = createMuiTheme({
  typography: {
    subtitle1: {
      fontSize: 14,
    },
  },
});
const useStyles = makeStyles({
  root: {
    /*minWidth: 275,*/
    width: "275px",
    height: "168px",
    boxShadow: "0 8px 15px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 9px 30px -12.125px rgba(0,0,0,0.3)"
    },
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const titulo = "Secretarías";
const subtitulo = "Listado de unidades dependientes";
const Secretarias = () => {
  const classes = useStyles();
  return (
    <div>
      <Box
        textAlign="center"
        fontWeight="fontWeightBold"
        fontSize="h6.fontSize"
        mt={2}
        mb={3}
      >
        {titulo}
        <ThemeProvider theme={theme}>
          <Typography variant="subtitle1">{subtitulo}</Typography>
        </ThemeProvider>
      </Box>
      <div className="cardSecre">
        <Card className={classes.root} variant="outlined">
            <div className="headercs">
            Secretaría de Planificación y Medio Ambiente
            </div>
            <div className="triangulo-equilatero-bottom-left">
                <div className="diagonalCS">sadfasd </div>
                <div className="diagonal">
                        ddd
                </div>
            </div>
        </Card>
      </div>
    </div>
  );
};
export default Secretarias;
