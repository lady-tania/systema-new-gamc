import React from "react";
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import "./style.css";

const titulo = "TRÁMITES GAMC";
const TramitesGamc = () => {
    return (
        <div>
            <Box textAlign="center" fontWeight="fontWeightBold" fontSize="h6.fontSize" mt={2} mb={3}>
                {titulo}
            </Box>
            <div>
                <div className="eye-lila">
                    <Box className="informacion" textAlign="left">
                        <Box textAlign="center" fontWeight="fontWeightBold" fontSize="h6.fontSize" pt={2} pb={3} className="titulo">
                            {titulo}
                        </Box>
                        Trabajamos para convertirnos en un municipio eficiente, transparente y accesible. Nuestro propósito es brindar servicios más efectivos, soluciones eficaces y rápidas.
                        <Box>
                        <Button
                            style={{
                                borderRadius: 8,
                                backgroundColor: "#4fb9a8",
                                padding: "10px 20px",
                                fontSize: "18px",
                                color: "#fff",
                                marginTop: "10px"
                            }}
                            variant="contained"
                        >
                            INGRESAR
                        </Button>
                        </Box>
                    </Box>
                </div>
                <div className="heaven-lila">
                    <div className='mydiv'></div>
                </div>
            </div>
        </div>
    );
};
export default TramitesGamc;