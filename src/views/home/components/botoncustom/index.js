import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Box from "@material-ui/core/Box";

import imgcar from "../images/gamc/camión.png";
import "./style.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 250,
    fontSize: "18px",
    boxShadow: "0 8px 30px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 9px 40px -12.125px rgba(0,0,0,0.3)",
    },
    "& .MuiCardMedia-root": {
      paddingTop: "56.25%",
    },
    "& .MuiTypography--heading": {
      fontWeight: "bold",
    },
    "& .MuiTypography--subheading": {
      lineHeight: 1.8,
    },
  },
});
const titulo = "GOBIERNO MUNICIPAL";

const listGobiernoMunicipal = [
  {
    id: 1,
    logo: imgcar,
    titulo: "BIENES Y SERVICIOS REQUERIDOS POR EL GAMC",
    descripcion: "Para ofertar productos a GAMC, ingrese a este enlace",
  },
];

const BotonCustom = () => {
  const classes = useStyles();
  return (
    <div>
      <Box
        textAlign="center"
        fontWeight="fontWeightBold"
        fontSize="h6.fontSize"
        mt={2}
        mb={3}
      >
        {titulo}
      </Box>
      {listGobiernoMunicipal.map((info) => (
        <Card className={classes.root} key={info.id} m={0.5}>
          <div className="mycard">
            <div className="headercard">
              <div className="iconheaderc">
                <img src={info.logo} alt="logo" />
              </div>
            </div>
            <CardContent>
            <Box fontWeight="fontWeightBold" fontSize={18} textAlign="center"> {info.titulo}</Box>
            </CardContent>
            <CardActions className="footerCard">
              <Box fontSize={14} m={1}>{info.descripcion}</Box>              
            </CardActions>
          </div>
        </Card>
      ))}
    </div>
  );
};
export default BotonCustom;
