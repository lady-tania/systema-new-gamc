import React from "react";
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';

import "./style.css";

const titulo = "Trámites GAMC";
const useStyles = makeStyles((theme) => ({
    button: {
      margin: theme.spacing(1),
    },
  }));
const Tramites = () => {
    const classes = useStyles();
    return (
        <div>
            <Box textAlign="center" fontWeight="fontWeightBold" fontSize="h6.fontSize" mt={2} mb={3}>
                {titulo}
            </Box>
            <div className="fondoTramites">
                <div className="infTramites">
                    <Box textAlign="center" className="title" fontWeight="fontWeightBold" fontSize="h6.fontSize" mt={2} mb={3}>
                        {titulo}
                    </Box>
                    <Box>
                        Trabajamos para convertirnos en un municipio eficiente, transparente y accesible. Nuestro propósito es brindar 
                        servicios más efectivos, soluciones eficaces y rápidas.
                    </Box>                   
                    <Button
                            style={{
                                borderRadius: 8,
                                backgroundColor: "#dd547c",
                                padding: "10px 20px",
                                fontSize: "18px",
                                color: "#fff",
                                marginTop: "20px",
                                textTransform: "capitalize"
                            }}
                            variant="contained"
                        >
                            <DeleteIcon />
                            Ingresar
                    </Button>
                </div>
            </div>
        </div>
    );
};
export default Tramites;