import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from '@material-ui/core/CardMedia';
import Button from "@material-ui/core/Button";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import WatchLaterIcon from '@material-ui/icons/WatchLater';

import imgfondo from "../images/gamc/noticia.jpg";
import "./style.css";
import { Height } from "@material-ui/icons";

const useStyles = makeStyles({
  root: {
    maxWidth: 300,
    fontSize: "18px",
    boxShadow: "0 8px 30px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 9px 40px -12px rgba(0,0,0,0.3)",
    },
    "& .MuiCardMedia-root": {
      paddingTop: "56.25%",
    },
    "& .MuiTypography--heading": {
      fontWeight: "bold",
    },
    "& .MuiTypography--subheading": {
      lineHeight: 1.8,
    },
  },
});

const titulo = "Últimas noticias";
const subtitulo = "Listado de unidades dependientes";
const BotonCustonNoticia = () => {
  const classes = useStyles();
  return (
    <div>
      <Box
        textAlign="center"
        fontWeight="fontWeightBold"
        fontSize="h6.fontSize"
        mt={2}
        mb={3}
      >
        {titulo}
      </Box>
      <div className="cardNews">
        <Card className={classes.root} m={0.5}>
          <CardMedia
            className={classes.media}
            image={imgfondo}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Box fontWeight="fontWeightBold" fontSize={18}>Alcaldía reapertura los parques infantiles</Box>
            <Box fontSize={14} style={{ color: "#6f6f6e", marginTop: "15px"}}>
                <WatchLaterIcon style={{ height: "14px" }}/>
                Publicado el 12 de abril de 2021
            </Box>
          </CardContent>
          <CardActions>
              <Button color="secondary" style={{ padding: "6px 20px", color: "#ea547c", textTransform: "capitalize"}}>Leer Más</Button>
          </CardActions>
        </Card>
      </div>
    </div>
  );
};
export default BotonCustonNoticia;
