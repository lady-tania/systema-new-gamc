import React from "react";
import Box from '@material-ui/core/Box';
import "./style.css";

const titulo = "Organigrama";

const Organigrama = () => {
    return (
        <div>
            <Box textAlign="center" fontWeight="fontWeightBold" fontSize="h6.fontSize" mt={2} mb={3}>
                {titulo}
            </Box>

            <div className="tree">
                <ul>
                    <li>
                        <a href="#">Padre</a>
                        <ul>
                            <li>
                                <a href="#">Hijo</a>
                                <ul>
                                    <li>
                                        <a href="#">Nieto</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Hijo</a>
                                <ul>
                                    <li><a href="#">Nieto</a></li>
                                    <li>
                                        <a href="#">Nieto</a>
                                        <ul>
                                            <li>
                                                <a href="#">Bisnieto</a>
                                            </li>
                                            <li>
                                                <a href="#">Bisnieto</a>
                                            </li>
                                            <li>
                                                <a href="#">Bisnieto</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Nieto</a></li>
                                    <li><a href="#">Nieto</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Hijo</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    );
};
export default Organigrama;