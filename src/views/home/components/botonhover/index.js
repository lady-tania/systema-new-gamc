import React from "react";

import "./style.css";

const BotonHover = () => {
  return (
    <div>
      <ul className="menu" data-animation="center">
        <li>
          <a href="#0">About</a>
        </li>
        <li>
          <a href="#0">Projects</a>
        </li>
        <li>
          <a href="#0">Clients</a>
        </li>
        <li>
          <a href="#0">Contact</a>
        </li>
      </ul>
    </div>
  );
};
export default BotonHover;
