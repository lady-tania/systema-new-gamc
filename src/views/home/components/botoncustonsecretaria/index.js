import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from '@material-ui/core/CardMedia';
import Button from "@material-ui/core/Button";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import imgfondo from "../images/gamc/fondoSecretaria.jpg";
import "./style.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 250,
    fontSize: "18px",
    boxShadow: "0 8px 30px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 9px 40px -12px rgba(0,0,0,0.3)",
    },
    "& .MuiCardMedia-root": {
      paddingTop: "56.25%",
    },
    "& .MuiTypography--heading": {
      fontWeight: "bold",
    },
    "& .MuiTypography--subheading": {
      lineHeight: 1.8,
    },
  },
});

const titulo = "Secretarías";
const subtitulo = "Listado de unidades dependientes";
const BotonCustonSecretaria = () => {
  const classes = useStyles();
  return (
    <div>
      <Box
        textAlign="center"
        fontWeight="fontWeightBold"
        fontSize="h6.fontSize"
        mt={2}
        mb={3}
      >
        {titulo}
      </Box>
      <div className="cardSecre">
        <Card className={classes.root} m={0.5}>
          <CardMedia
            className={classes.media}
            image={imgfondo}
            title="Contemplative Reptile"
          />

          <CardContent>
            <Box fontWeight="fontWeightBold" fontSize={18}> Secretaría General</Box>
          </CardContent>
          <CardActions>
            <div className="footerCS">
              <Button
                style={{
                  borderRadius: 5,
                  backgroundColor: "#00acd8",
                  fontSize: "15px",
                  color: "#fff",
                  padding: "6px 20px",
                  textTransform: "capitalize"
                }}
                variant="contained"
              >                
                Visitar
                <div className="iconCS">
                  <ChevronRightIcon style={{ marginTop: "3px" }}/>
                </div>
              </Button>
              </div>
          </CardActions>
        </Card>
      </div>
    </div>
  );
};
export default BotonCustonSecretaria;
