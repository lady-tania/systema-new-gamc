import React from 'react';
// import GobiernoMunicipal from './components/gobiernomunicipal';
import Organigrama from './components/organigrama';
// import TramitesGamc from './components/tramitesgamc';
// import Secretarias from './components/secretarias';
import BotonCustonSecretaria from './components/botoncustonsecretaria';
import BotonCustonNoticia from './components/botoncustonnoticia';
import BotonHover from './components/botonhover';

import BotonCustom from './components/botoncustom';
import Tramites from './components/tramites';
const Home = () => {
    return (
        <div>
            <BotonHover />
            <BotonCustom />
            <Tramites />
            <BotonCustonSecretaria />
            <BotonCustonNoticia /> 
            <Organigrama />
        </div>
    );
}
export default Home;